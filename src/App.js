import logo from './logo.svg';
import './App.css';
import {Route,Switch} from 'react-router-dom'
import SideBar from './Component/TP4/SideBar'
import ListRepos from "./Component/ListRepos"
import TP3 from './Component/tp3/App3'
import Calculator from './Component/TP1/Calculator'
import App3 from './Component/tp22/App2'
import TodoList from './Component/TodoList/TodoContainer'
import App5 from './Component/TP5/App'

function App() {
  return (
    <div className="App">
       <SideBar/>
       <Switch>
         <Route exact path='/TP2' render={()=><App3/>}/>
         <Route exact path='/TP1' component={Calculator} />
         <Route exact path='/TP3' component={TP3} />
         <Route exact path='/TP5' component={App5} />

       </Switch>
    </div>
  );
}

export default App;
