import React, {useState} from 'react'
import Todo from './Todo'
import TodoFormContoller from './TodoFormContoller'

export default function TodoContainer() {

    const [textTodo, setTextTodo] = useState("");
    const [todos, setTodos] = useState([]);
    function AddItem(){
            
           setTodos([...todos, textTodo]);  
    }
    console.log(todos);
    return (
        <div>
            <TodoFormContoller
                addFunction={AddItem}
                textTodo={textTodo}
                setText={setTextTodo}
            />
            { <Todo todoList={todos}/> }
        </div>
    )
}
