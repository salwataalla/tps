import React,{useState} from 'react'


export default function TodoFormContoller(props) {
   function onChangeTodo(event) {
       const value=event.target.value;
       props.setText(value);
       
   }
    return (
        <div className="mb-5">
           <input
           value={props.textTodo}
           onChange={ (event) => onChangeTodo(event) }
           className="form-control"
           type="text" 
           placeholder="Add item"
           />
           <button onClick={props.addFunction}>
               Add ToDO
           </button>
        </div>
    )
}
