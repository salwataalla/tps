import React,{useState} from 'react';
import './card.css';
function Card({avatar,name,stars,open_issues,description}) {
 
  return (
    <div className='card'>
       
       <div className="card_image">
    <img src={avatar} className="card_avatar"/>
        </div>
  <div className="card_body">
    <h2 className="cardtitle">{name} </h2>
     <p class="content"> {description} </p>
     <button type="button" className="btn btn-warning">Stars: {stars}</button>
     <button type="button" className="btn btn-info">Open issues : {open_issues}</button>
     <span className='submition'> </span>

  </div>
  </div>    
  );
}

export default Card;
