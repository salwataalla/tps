import React from 'react';
import Container from "./Container";


function FormControl(props){
    return ( 
        <Container
         bgd={props.bg}
        >
        <label>{props.textLabel}</label>
        <input
          onChange={function (event) {
            props.onChangeFunction(event);
          }}
          value={props.value ?? ""}
          type={props.type}
          placeholder={props.placeholder}
        />
      </Container>

    )
}
 export default FormControl;