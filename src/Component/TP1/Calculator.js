import React, { useState } from "react";
import FormControl from './FormContol';


function Calculator() {
  const [poid, setPoid] = useState(0);
  const [taille, setTaille] = useState(0);
  const [IMC, setIMC] = useState(0);
  

  function calculIMC() {
    setIMC(poid / taille**2);
  }

  function onChangePoid(event) {
    const value = event.target.value;
    setPoid(value);
  }

  function onChangeTaille(event) {
    const value = event.target.value;
    setTaille(value);
  }
  function checkIMC (Imc){
    if(Imc<18.5){ return "insuffisance" }
    if(Imc>18.5 && Imc<25){return "poids normal" }
    if(Imc>25&& Imc<29.9){return "surpoids"}
    if(Imc>30){return "obesite" }
    else return 'hahaha';
  }
  function renitialiser(){
    setPoid(null);
    setTaille(null);
    setIMC(0);
  }

  return (
    <div className="container bg-warning">
      <h1>Calcul IMC</h1>
      <div>
        <form>
          {/* <div>
            <label>Poid (kg) {poid}</label>
            <input
              onChange={function (event) {
                onChangePoid(event);
              }}
              value={poid ?? ""}
              type="text"
              placeholder="entrer le poid"
            />
          </div> */}
      <FormControl
        bg="bg-danger"
        textLabel='poid en kg'
        type='text'
        placeholder='entrer le poid'
        onChangeFunction={onChangePoid}
        value={poid}
      />



          {/* <div>
            <label>Taille (m) {taille}</label>
            <input
              onChange={function (event) {
                onChangeTaille(event);
              }}
              value={taille ?? ""}
              type="text"
              placeholder="entrer la taille"
            />
          </div> */}
            <FormControl
            bg="bg-success"
        textLabel='taille  (m)'
        type='text'
        placeholder='entrer la taille'
        onChangeFunction={onChangeTaille}
        value={taille}
      />

          <div>
            <button type="button" onClick={renitialiser}>rénitialiser</button>
            <button type="button" onClick={calculIMC}>
              calculer
            </button>
          </div>
        </form>

        <div>{IMC === 0 ? "" : <h3>IMC : {IMC.toFixed(2)}</h3>}</div>

        <div>{IMC === 0 ? "" : <h3>etat :  {checkIMC(IMC)}</h3>}</div>

      </div>
    </div>
  );
}

export default Calculator;

