import React from 'react';

function Container(props){
    return(
        <div className={props.bgd}>
        {props.children}
      </div>
    )
}
export default Container;