import React, { useEffect,useState } from 'react';
import axios from 'axios';
import Repo from './Repo';
function ListRepos() {
    const [repos, setRepos] = useState([]);

    useEffect(
        function () {
            const fn = async () => {
                const reponse = await axios.get("https://api.github.com/search/repositories?q=created:>2020-11-05&sort=stars&order=desc&page=2");
                setRepos(reponse.data.items);
                console.log(reponse.data.items)
            }
            fn();
        },
        function () {

        },
        [])

    return (
        <div>  
            {
              repos &&  repos.map((item)=>{
                   return  <Repo 
                   data={item}
                   />
                })
            }
           
        </div>
        
    )
}
export default ListRepos;