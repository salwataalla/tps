import React from 'react'


function Repo(props) {
    return (
        <div class="card" style={{width: "18rem"}}>
            <img class="card-img-top" src={props.data.owner.avatar_url} alt="Card image cap" />

            <div class="card-body">
                <h1> {props.data.name} </h1>
                <p class="card-text">{props.data.description}</p>
                <span className="bg-warning">star : {props.data.forks}</span><br/>
                <small className="bg-success">open issues :{props.data.open_issues}</small><br/>
                <small>date :{props.data.created_at.split("T")[0]}</small>
            </div>
        </div>
    )
}
export default Repo;
