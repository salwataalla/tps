import React,{Component} from 'react';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      newItem:"",
      list:[]
    }
  }
  updateInput(key, value){
    //update react state
    this.setState({
      [key]:value
    });
  }
  deleteItem(id){
    //copy current list of items
    const list = [...this.state.list];
    // filter  out item being deleted
    const updatedList =list.filter(item => item.id !== id );
    this.setState({list : updatedList});
  }
  addItem(){
    //create item with unique id
    const newItem={
      id : 1 + Math.random(),
      value: this.state.newItem.slice()
    };
    //copy of current list of items
    const list = [...this.state.list];
    //Add new item to list
    list.push(newItem);
    //update state with new list and reset newItem input
    this.setState({
      list,
      newItem:""
    });
  }
  render(){
  return (
  <div className="test">
    <div>
        Add TODO...
        <br/>
      <input 
            type="text" 
            placeholder="Type item here..."
            value={this.state.newItem}
            onChange={e => this.updateInput("newItem", e.target.value)}
      />
      <button
       className="btn btn-primary btn-sm ml-3"
            onClick={() => this.addItem()}
      >
        Add
      </button>
         <br/>
          <ul>
            {this.state.list.map(item=> {
              return (
                <li key={item.id}>
                  {item.value}
                  <button className="btn btn-primary btn-sm ml-3"
                  onClick={() => this.deleteItem(item.id)}
                  >
                      Supprimer
                  </button>
                  <button className="btn btn-primary btn-sm ml-3"
                  onClick={() => this.updateInput(item.id)}
                  >
                      Modifier
                  </button>
                </li>
              )
            })
            }
          </ul>
    </div>
  </div>
  );
  }
}

export default App;
